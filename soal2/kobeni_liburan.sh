#!/bin/bash

current_hour=$(date +"%H")

#crontab -> 0 0 * * * /.../.../kobeni_liburan.sh -z
func_zip() {
	n=$(find . -type f -name "*devil*" | grep -c devil)
	#x=$(find . -type f -name "*kumpulan*" | grep -c kumpulan)
	zip -r devil_`expr $n + 1`.zip kumpulan_1 kumpulan_2
	rm -rf kumpulan_1 kumpulan_2
}

#crontab -> 0 */10 * * * /.../.../kobeni_liburan.sh -d
download_photo() {
	n=$(find . -type f -name "*kumpulan*" | grep -c kumpulan)
	mkdir kumpulan_`expr $n + 1`
	if [ $current_hour -ne 0 ];
	then
		for ((num=1; num<=$current_hour; num=num+1));
		do
		wget -O perjalanan_"$num".jpg https://loremflickr.com/320/240/indonesia
		mv perjalanan_* kumpulan_`expr $n + 1`
		done
	else
		wget -O perjalanan_1.jpg https://source.unsplash.com/random/100x100/?indonesia
		mv perjalanan_* kumpulan`expr $n + 1`
	fi
}

case $1 in
	-d)
	download_photo
	;;
	-z)
	func_zip
	;;
esac

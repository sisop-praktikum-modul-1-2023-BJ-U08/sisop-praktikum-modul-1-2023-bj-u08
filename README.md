# sisop-praktikum-modul-1-2023-BJ-U08



## Group Members

Mochammad Naufal Ihza Syahzada      5025211260

Ariel Pratama Menlolo               5025211194

Teuku Aulia Azhar                   5025201142

## Task Check

- [x] soal1 (1b revisi)
- [x] soal2 (revisi di demo)
- [x] soal3
- [x] soal4 (revisi)

link revisi [Video Revisi](https://drive.google.com/drive/folders/1t0pMFfUtAvOJpb0gsMEuTNypufw5sWXD?usp=sharing)

## 

```
cd existing_repo
git remote add origin https://gitlab.com/sisop-praktikum-modul-1-2023-BJ-U08/sisop-praktikum-modul-1-2023-bj-u08.git
git branch -M main
git push -uf origin main
```

# Explanation

## soal1

### 1a) Find the top 5 Japan University
```
awk -F',' '/Japan/ {for (i=1; i<=NR; i++) printf "%s\t", $i; printf"\n"}' 2023\ QS\ World\ University\ Rankings.csv | head -n 5
```

code breakdown:
1. **-F','** 
 
> A function to separate between sentence. As we know, in general linux will separate it when there's a space. But in this case since it was separated by ',' (coma), we use this -F function 
2. **/Japan/**
 
> This is a way to sort data that have word "Japan" in a column
3. **for (i=1; i<=NR; i++) printf "%s\t", $i**
 
> Print every data in a column and give a space using tab. NR is the number of column on the file
4. **printf"\n"**
 
> Give a new line space every end of the row
5. **2023\ QS\ World\ University\ Rankings.csv**
 
> Call out the file that will be used
6. **| head -n 5**
 
> Pipe symbol that redirects the output of the awk command to the input of some command and head will sort the data from top and will be limited to only five data

```
output:

23      The University of Tokyo JP      Japan   100     7       99.7    9       91.9    78      73.3    12810.4     601+    27.8    448     89.5    174     97.8    33      85.3
36      Kyoto University        JP      Japan   98.6    21      98.9    15      94.8    62      54.2    23414.9     601+    22.1    503     85.5    233     56.9    201     81.4
55      Tokyo Institute of Technology (Tokyo Tech)      JP      Japan   74.1    80      93.4    42      81.126      65.9    168     36.1    433     37.9    362     55.8    601+    33.6    377     72.5
68      Osaka University        JP      Japan   80.2    68      85.4    61      67.4    205     59.1    20625       525     14.4    601+    75.2    385     21      567     68.2
79      Tohoku University       JP      Japan   71.8    86      78.1    75      98.6    43      34.2    40114.1     601+    16.4    595     66.8    493     18.7    601+    64.9
```

### 1b) Find university with the lowest FSR score from that top 5 Japan university
```
awk -F',' '/Japan/ {printf "%.1f\t%s\t", $9, $2; printf"\n"}' 2023\ QS\ World\ University\ Rankings.csv | head -5 | sort | head -1
```

code breakdown:
1. **-F','**
 
> A function to separate between sentence. As we know, in general linux will separate it when there's a space. But in this case since it was separated by ',' (coma), we use this -F function 
2. **/Japan/**
 
> This is a way to sort data that have word "Japan" in a column
3. **printf "%.1f\t%s\t", $9, $2**
 
> This will print a float data with 1 number after "." and print a string variable. Those two data are from two column, ninth column and second column 
4. **printf"\n"**
 
> Give a new line space every end of the row
5. **2023\ QS\ World\ University\ Rankings.csv**
 
> Call out the file that will be used
6. **| head -5 | sort | head -1**
 
> Pipe symbol that redirects the output of the awk command to the input of some command. And head will sort the data from top and will be limited to only five data. After the program will only 5 data, those data will be sorted. And since the question only want the lowest one, we need another head -1, so it only show the lowest data.

```
output:

67.4    Osaka University
```


### 1c) Top 10 Japan university with the highest ger rank
```
awk -F',' '/Japan/ {printf "%s\t%s\t", $20, $2; printf"\n"}' 2023\ QS\ World\ University\ Rankings.csv | sort -n | head -10
```
code breakdown:
1. **-F','**

> A function to separate between sentence. As we know, in general linux will separate it when there's a space. But in this case since it was separated by ',' (coma), we use this -F function 
2. **/Japan/**

> This is a way to sort data that have word "Japan" in a column
3. **printf "%s\t%s\t", $20, $2**

> This will print two strings variable. Those two data are from two column, 20nd column and second column
4. **printf"\n"**

> Give a new line space every end of the row
5. **2023\ QS\ World\ University\ Rankings.csv**

> Call out the file that will be used
6. **| sort -n | head -10**

> Pipe symbol that redirects the output of the awk command to the input of some command. First command is sort based on numerical type. And then head will sort the data from top and will be limited to only ten data.

```
output:

33      The University of Tokyo
90      Keio University
127     Waseda University
169     Hitotsubashi University
188     Tokyo University of Science
201     Kyoto University
367     Nagoya University
377     Tokyo Institute of Technology (Tokyo Tech)
379     International Christian University
543     Kyushu University
```

### 1d) Find university with "paling keren" keyword
```
awk -F',' '{if($2 ~ /Keren/) for (i=1; i<=NR; i++) printf "%s\t", $i;}' 2023\ QS\ World\ University\ Rankings.csv
```
code breakdown:
1. **-F','**

> A function to separate between sentence. As we know, in general linux will separate it when there's a space. But in this case since it was separated by ',' (coma), we use this -F function 
2. **/Japan/**

> This is a way to sort data that have word "Japan" in a column
3. **if($2 ~ /Keren/)**

> Find a data that has "Keren" word in a column
4. **for (i=1; i<=NR; i++)**

> Inside the if function, there is this for loop function that used for print every column in a row
5. **printf "%s\t", $i;**

> Print a string variable from $i, and var i is from the for loop before
6. **2023\ QS\ World\ University\ Rankings.csv**

> Call out the file that will be used

```
output:

711     Institut Teknologi Sepuluh Nopember (ITS Surabaya Keren)        ID      Indonesia       13      50130.3     322     30.7    503     1.8     601+    60      307     2.9     601+    12.2    601+    19.8    595n
```


## soal2

```
current_hour=$(date +"%H")
```
> find the hour and save it to a variable called current_hour

```
#crontab -> 0 0 * * * /.../.../kobeni_liburan.sh -z
func_zip() {
	n=$(find . -type f -name "*devil*" | grep -c devil)
	#x=$(find . -type f -name "*kumpulan*" | grep -c kumpulan)
	zip -r devil_`expr $n + 1`.zip kumpulan_1 kumpulan_2
	rm -rf kumpulan_1 kumpulan_2
}
```
> a function to make a zip. Variable n is the number of file that located in the file (named devil). Then there's a zip command that will zip kumpulan_ folder and will named it devil_ . The last command is rm, this command will remove all kumpulan_ folder that already got zipped.
> crontab will be set every 24 hours (you can see the cronjob command in the first line that start with #)

```
#crontab -> 0 */10 * * * /.../.../kobeni_liburan.sh -d
download_photo() {
	n=$(find . -type f -name "*kumpulan*" | grep -c kumpulan)
	mkdir kumpulan_`expr $n + 1`
	if [ $current_hour -ne 0 ];
	then
		for ((num=1; num<=$current_hour; num=num+1));
		do
		wget -O perjalanan_"$num".jpg https://loremflickr.com/320/240/indonesia
		mv perjalanan_* kumpulan_`expr $n + 1`
		done
	else
		wget -O perjalanan_1.jpg https://source.unsplash.com/random/100x100/?indonesia
		mv perjalanan_* kumpulan`expr $n + 1`
	fi
}
```
> a function to download the photo. Command mkdir will make a directory or folder. Jump into the if else statement, inside if statement there is a loop that used to download photo and move it to kumpulan_ folder. The else statement will do the same (download and move) but since there's a condition where when the clock is at 00:00 that it the code will only print 1 photo.

```
case $1 in
        -d)
        download_photo
        ;;
        -z)
        func_zip
        ;;
esac
```
> execute the function by using the case statement. $1 will took from the input itself and in this case you can choose either you want to print or zip by adding -d or -z.

## soal3

### 3a) Make a login and register program whose passwords have several conditions
**_REGISTER_** _louis.sh_
```
echo "REGISTER"
echo "INPUT ID: "
read userid
echo "INPUT PASS: "
read userpass
```

> By this the user can see where to input the ID and Password. And "read" is for scan the user input

```
#1
if [ ${#userpass} -lt 8 ]; then
echo "Wrong Password Format, Password minimal terdiri dari 8 kombinasi huruf dan angka!"
exit 1
fi

#2
if ! [[ $userpass =~ [A-Z] && $userpass =~ [a-z] && $userpass =~ [0-9] ]]; then
echo "Wrong Password Format, Password minimal terdiri atas huruf besar, huruf kecil ,dan angka!"
exit 1
fi

#3
if [ "$userid" == "$userpass" ]; then
echo "Wrong Password Format, ID dan Password tidak boleh sama!"
exit 1
fi

#4
if echo "$userpass" | grep -qi "chicken"; then
echo "gabole gitu passwordnya :("
exit 1
fi

#5
if echo "$userpass" | grep -qi "ernie"; then
echo "gabole gitu passwordnya2 :("
exit 1
fi

```
> Since there are some rules for password, we use if statement to fulfill them. The first if statement are used to check the length. Second if, is used to check the alphanumeric rule. Third if, is used to check the id and password are the same or not. Finally the last two if, are used to check if the password contain "chicken" or "ernie".  

```
existid=$(awk -v input="$userid" '$1 ~ "\\<"input"\\>" {++n} END {print n}' /users/users.txt)
```
> Declare a variable that is coming from .txt file, where the value are used to check if the ID already exist or not.

```
case "$existid" in
"1")
echo "ID already taken"
echo $(date +"%y/%m/%d %T") "REGISTER: ERROR User Already Exists" >> logs.txt
exit 1
;;
*)
echo "Akun Telah Dibuat!"
echo "$userid $userpass" >> /users/users.txt
echo $(date +"%y/%m/%d %T") "REGISTER: INFO User $userid registered sucessfully" >> logs.txt
;;
esac
```
> Switch case to check the previous case. So if existid equals to 1, it means that the ID already taken and it will print some information to a .txt file. When the value is other than one, it means that the ID is not taken and the program can input it to the database (also the password). Also it will print an information to a .txt file.


### 3b) Everytime there's an activity on login or register, it must be recorded on logs.txt file

**_LOGIN_** _retep.sh_

```
echo "LOGIN"
echo "ID: "
read iduser
echo "Pass: "
read passuser
```
> An output that made for user to input their registered ID and Password

```
n=$(awk -v inputa="$iduser" -v inputb="$passuser" '$1 ~ "\\<"inputa"\\>" && $2 ~ "\\<"inputb"\\>" {++n} END {print n}' /users/users.txt)

```
> a variable that will contain a int value, where this value are from checking the .txt file. When the ID and Password are match, the value will be 1. Other than that, it means that the ID and Password doesn't match.

```
case "$n" in
"1")
echo "Login Berhasil"
echo $(date +"%y%m%d %T") "LOGIN: INFO User $iduser logged in" >> logs.txt
;;
*)

if [ ${#passuser} -lt 8 ]; then
echo "Wrong Password Format, Password minimal terdiri dari 8 kombinasi huruf dan angka!"
fi

if ! [[ $passuser =~ [A-Z] && $passuser =~ [a-z] && $passuser =~ [0-9] ]]; then
echo "Wrong Password Format, Password minimal terdiri atas huruf besar, huruf kecil, dan angka!"
fi

if [ "$passuser" == "$iduser" ]; then
echo "Wrong Password Format, ID dan Password tidak boleh sama!"
fi

if echo "$passuser" | grep -qi "chicken"; then
echo "gabole gitu passwordnya"
fi

if echo "$passuser" | grep -qi "ernie"; then
echo "gabole gitu passwordnya"
fi

echo "ID atau Password yang anda masukkan SALAH"
echo $(date +"%y/%m/%d %T") "LOGIN: ERROR Failed login attempt on user $iduser" >> logs.txt
;;
esac

```
> the first case will run when n is equals to 1, and the program will tell the user that the login is working.
> the other case will run when n is not equals to 1, and it will check the problem by using the if statement (those if statements are the same)
> On the last line in every case, the program will sent an information to a .txt file with specific format

## soal4

Code description already included in the code

**ENCRYPT**

```
#!/bin/bash
# cronjobs:
# 0 */2 * * * bash /.../log_encrypt.sh

# converts an ASCII code to a character
chr() {
  printf \\$(printf '%03o' $1)
}

# variable that contains the current date and time, each variable has their own specific format
fileName=$(date +"%H:%M %d:%m:%Y")
hourNow=$(date +"%H")

# calculates ASCII code for lower and uppercase of the current hour
lowerCaseAsciiHour=$(chr $((hourNow + 97)))
upperCaseAsciiHour=$(chr $((hourNow + 65)))

# read the contents of a file and pipes output to two tr. The subs replaces all occurances of the letters in the source
awk '{print $0}' /var/log/syslog | tr [a-z [$lowerCaseAsciiHour-za-$lowerCaseAsciiHour] | tr [A-Z [$upperCaseAsciiHour-ZA-$upperCaseAsciiHour] >~/backup/syslog/"$fileName".txt

```

**DECRYPT**
```
#!/bin/bash
# cronjobs:
# 0 */2 * * * /.../log_decrypt.sh

# converts an ASCII code to a character
chr() {
  printf \\$(printf '%03o' $1)
}

# variable that contains the current date and time, each variable has their own specific format
hourNow=$(date +"%H")
fileName=$(date +"%H:%M %d:%m:%Y")

# calculates ASCII code for lower and uppercase of the current hour
lowerCaseAsciiHour=$(chr $((97 + 26 - $hourNow)))
upperCaseAsciiHour=$(chr $((65 + 26 - $hourNow)))

# reads the contents of the file, pipes the output to two tr commands. The substitution replaces all occurrences of the letters. Finally, the output is redirected to a file called result.txt.
awk '{print $0}' ~/backup/syslog/"$fileName".txt | tr [a-z [$lowerCaseAsciiHour-za-$lowerCaseAsciiHour] | tr [A-Z [$upperCaseAsciiHour-ZA-$upperCaseAsciiHour] >result.txt

```

#!/bin/bash
# 0 */2 * * * /.../log_decrypt.sh

# converts an ASCII code to a character
chr() {
  printf \\$(printf '%03o' $1)
}

# variable that contains the current date and time, each variable has their own specific format
hourNow=$(date +"%H")
fileName=$(date +"%H:%M %d:%m:%Y")

# calculates ASCII code for lower and uppercase of the current hour
lowerCaseAsciiHour=$(chr $((97 + 26 - $hourNow)))
upperCaseAsciiHour=$(chr $((65 + 26 - $hourNow)))

# reads the contents of the file, pipes the output to two tr commands. The substitution replaces all occurrences of the letters. Finally, the output is redirected to a file called result.txt.
awk '{print $0}' ~/backup/syslog/"$fileName".txt | tr [a-z [$lowerCaseAsciiHour-za-$lowerCaseAsciiHour] | tr [A-Z [$upperCaseAsciiHour-ZA-$upperCaseAsciiHour] >result.txt

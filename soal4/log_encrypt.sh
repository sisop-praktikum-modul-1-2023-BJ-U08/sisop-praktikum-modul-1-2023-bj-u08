#!/bin/bash
# cronjobs:
# 0 */2 * * * bash /.../log_encrypt.sh

# converts an ASCII code to a character
chr() {
  printf \\$(printf '%03o' $1)
}

# variable that contains the current date and time, each variable has their own specific format
fileName=$(date +"%H:%M %d:%m:%Y")
hourNow=$(date +"%H")

# calculates ASCII code for lower and uppercase of the current hour
lowerCaseAsciiHour=$(chr $((hourNow + 97)))
upperCaseAsciiHour=$(chr $((hourNow + 65)))

# read the contents of a file and pipes output to two tr. The subs replaces all occurances of the letters in the source
awk '{print $0}' /var/log/syslog | tr [a-z [$lowerCaseAsciiHour-za-$lowerCaseAsciiHour] | tr [A-Z [$upperCaseAsciiHour-ZA-$upperCaseAsciiHour] >~/backup/syslog/"$fileName".txt

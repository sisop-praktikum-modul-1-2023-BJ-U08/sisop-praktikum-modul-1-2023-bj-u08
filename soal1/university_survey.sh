#!/bin/bash

printf "1a. 5 Universitas dengan ranking tertinggi:\n\n";
awk -F',' '/Japan/ {for (i=1; i<=NR; i++) printf "%s\t", $i; printf"\n"}' 2023\ QS\ World\ University\ Rankings.csv | head -n 5
printf "\n1b. FSR Score paling rendah:\n\n";
awk -F',' '/Japan/ {printf "%.1f\t%s\t", $9, $2; printf"\n"}' 2023\ QS\ World\ University\ Rankings.csv | head -5 | sort | head -1
printf "\n1c. 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi:\n\n";
awk -F',' '/Japan/ {printf "%s\t%s\t", $20, $2; printf"\n"}' 2023\ QS\ World\ University\ Rankings.csv | sort -n | head -10
printf "\n1d. Universitas paling keren di dunia:\n\n";
awk -F',' '{if($2 ~ /Keren/) for (i=1; i<=NR; i++) printf "%s\t", $i;}' 2023\ QS\ World\ University\ Rankings.csv

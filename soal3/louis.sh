#!/bin/bash

echo "REGISTER"
echo "INPUT ID: "
read userid
echo "INPUT PASS: "
read userpass

if [ ${#userpass} -lt 8 ]; then
echo "Wrong Password Format, Password minimal terdiri dari 8 kombinasi huruf dan angka!"
exit 1
fi

if ! [[ $userpass =~ [A-Z] && $userpass =~ [a-z] && $userpass =~ [0-9] ]]; then
echo "Wrong Password Format, Password minimal terdiri atas huruf besar, huruf kecil ,dan angka!"
exit 1
fi

if [ "$userid" == "$userpass" ]; then
echo "Wrong Password Format, ID dan Password tidak boleh sama!"
exit 1
fi

if echo "$userpass" | grep -qi "chicken"; then
echo "gabole gitu passwordnya :("
exit 1
fi

if echo "$userpass" | grep -qi "ernie"; then
echo "gabole gitu passwordnya2 :("
exit 1
fi

existid=$(awk -v input="$userid" '$1 ~ "\\<"input"\\>" {++n} END {print n}' /users/users.txt)

case "$existid" in
"1")
echo "ID already taken"
echo $(date +"%y/%m/%d %T") "REGISTER: ERROR User Already Exists" >> logs.txt
exit 1
;;
*)
echo "Akun Telah Dibuat!"
echo "$userid $userpass" >> /users/users.txt
echo $(date +"%y/%m/%d %T") "REGISTER: INFO User $userid registered sucessfully" >> logs.txt
;;
esac

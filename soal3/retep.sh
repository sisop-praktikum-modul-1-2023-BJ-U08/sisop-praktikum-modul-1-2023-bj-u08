#!/bin/bash

echo "LOGIN"
echo "ID: "
read iduser
echo "Pass: "
read passuser
n=$(awk -v inputa="$iduser" -v inputb="$passuser" '$1 ~ "\\<"inputa"\\>" && $2 ~ "\\<"inputb"\\>" {++n} END {print n}' /users/users.txt)

case "$n" in
"1")
echo "Login Berhasil"
echo $(date +"%y%m%d %T") "LOGIN: INFO User $iduser logged in" >> logs.txt
;;
*)

if [ ${#passuser} -lt 8 ]; then
echo "Wrong Password Format, Password minimal terdiri dari 8 kombinasi huruf dan angka!"
fi

if ! [[ $passuser =~ [A-Z] && $passuser =~ [a-z] && $passuser =~ [0-9] ]]; then
echo "Wrong Password Format, Password minimal terdiri atas huruf besar, huruf kecil, dan angka!"
fi

if [ "$passuser" == "$iduser" ]; then
echo "Wrong Password Format, ID dan Password tidak boleh sama!"
fi

if echo "$passuser" | grep -qi "chicken"; then
echo "gabole gitu passwordnya"
fi

if echo "$passuser" | grep -qi "ernie"; then
echo "gabole gitu passwordnya"
fi

echo "ID atau Password yang anda masukkan SALAH"
echo $(date +"%y/%m/%d %T") "LOGIN: ERROR Failed login attempt on user $iduser" >> logs.txt
;;
esac

